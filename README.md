# In�s Daniel & Gui

[ines-daniel-gui.party](http://ines-daniel-gui.party/)

Static website for In�s and Daniel's wedding + Gui's baptism

### Get Started

- Clone repo: `git clone https://anacarlos@bitbucket.org/anacarlos/ines-daniel-gui.git`
- Install dependencies: `npm install`
- Start server: `gulp browserSync`
- Deploy: `surge`

### Tech

- Bootstrap: [Creative template](http://startbootstrap.com/template-overviews/creative/)
- Deployed with [surge.sh](https://surge.sh/)
